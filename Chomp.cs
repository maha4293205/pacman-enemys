using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chomp : MonoBehaviour
{
    public float moveSpeed = 1f;
    private Rigidbody rb;
    private int score = 0;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        
    }

    private void Update()
    {

        float mh = Input.GetAxis("Horizontal");
        float mv = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(mh, 0f, mv);

        


        transform.Translate(movement * moveSpeed * Time.deltaTime);
    }


   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("coin"))
        {
            Destroy(other.gameObject);
            score++;
            Debug.Log(score);
            

        }
        else if (other.gameObject.tag.Equals("stage"))
        {
            Debug.Log("collider");
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

        }
        else if (other.gameObject.tag.Equals("cherry"))
        {
            Destroy(other.gameObject);
            score = score + 20;
            Debug.Log("+20");
            
        }
    }
    

}
