using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.AI;
public class Script : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform point;
    public NavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        //Agent set destination au point
           agent.SetDestination(point.position);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("chromp"))
        {
            Debug.Log("Game Over!");
            Application.Quit();
        }
    }


}